import Mouton #import la classe Mouton
import Monde #import la classe Monde
import random #import la classe aléatoire
import Loup #import la classe Loup
import Chasseur #import la classe Chasseur

class Simulation: #Cette classe va permettre de simuler le monde
    def __init__(self,nbr_m,fin_M,monde_d,monde_dr,m_max):
        self.nombre_moutons = nbr_m #on initialise le nombre de mouton
        self.horloge = 0 #on initialise l'horloge à 0 au debut de la simulation
        self.fin_du_monde = fin_M #on définit la fin du monde(de la simulation)
        self.mondes = Monde.Monde(monde_d,monde_dr) #on initialise le monde
        self.moutons = [Mouton.Mouton(10,random.randint(0,self.mondes.dimension),random.randint(0,self.mondes.dimension),6,self.mondes) for i in range(nbr_m)] #une liste de mouton
        self.loups = [Loup.Loup(18,random.randint(0,self.mondes.dimension),random.randint(0,self.mondes.dimension),5,self.mondes,self.moutons) for i in range((nbr_m+1)//8)] #un liste de loup
        self.chasseurs = [Chasseur.Chasseur(random.randint(0,self.mondes.dimension),random.randint(0,self.mondes.dimension),self.mondes,self.loups) for i in range(self.mondes.dimension//10)] # une liste de mouton
        self.resultats_herbe = [] #on créer une liste vide pour les herbes
        self.resultats_moutons = [] #on créer une liste vide pour les moutons
        self.resultats_loups = [] #on créer une liste vide pour les loups
        self.max_mouton = m_max #nombre de mouton max possible sur un monde
        
    def simMouton(self): #Cette méthode va simuler le monde
        
        while self.horloge < self.fin_du_monde: #si l'horloge est inférieur à la fin du monde, on continue
            self.horloge += 1 #on ajoute 1 a l'horloge
            self.mondes.herbePousse() #on fait pousser l'herbe du monde
            
            x = 0
            for i in self.moutons: #elle permet de faire manger de l'herbe au mouton sinon il perd de l'energie
                i.variationEnergie(i.position_x,i.position_y)
                if i.energie == 0: #on verifie si son energie n'est pas à 0, si il l'est on le supprime de la liste mouton
                    self.moutons.pop(x)
                x += 1
                
            for i in self.moutons: #on fait ce reproduire les moutons
                for j in self.moutons[self.moutons.index(i)+1:]:
                    if i.position_x == j.position_x and i.position_y == j.position_y:
                        if random.randint(0,100) <= i.taux_reproduction:
                            self.moutons.append(i.place_mouton(i.position_x,i.position_y))
                            
            for i in self.moutons: #fait déplacer les moutons
                i.deplacement()
                
            for i in self.loups: #fait déplacer les loups
                i.deplacement()
                
            for i in self.chasseurs: #fait déplacer les chasseurs
                i.deplacement()
            
            for i in self.chasseurs: #fait que les chasseurs avec 1 munition va essayer de tuer un loup sur la même case que lui
                mun = 1
                x = 0
                for j in self.loups:
                    if i.position_x == j.position_x and i.position_y == j.position_y:
                        essaie = random.randint(0,1)
                        if essaie == 1 and mun == 1:
                            self.loups.pop(self.loups.index(j))
                            mun -= 1
                            
            x = 0 #on fait manger des moutons au loup si ils sont sur la même case qu'un mouton
            for i in self.loups:
                z = i.variationEnergie()
                if z[1] != None:
                    self.moutons.pop(z[1])
                if i.energie == 0:
                    self.loups.pop(x)
                x += 1
            
            for i in self.loups: #on fait ce reproduit les loups sur la même case
                for j in self.loups[self.loups.index(i)+1:]:
                    if i.position_x == j.position_x and i.position_y == j.position_y:
                        if random.randint(0,100) < i.taux_reproduction:
                            self.loups.append(i.place_loup(i.position_x,i.position_y))
            
            self.resultats_herbe.append(self.mondes.nbHerbe()) #on récupère le nombre d'herbe
            self.resultats_moutons.append(len(self.moutons)) #on récupère le nombre de mouton
            self.resultats_loups.append(len(self.loups)) #on récupère le nombre de loup
            
            if len(self.moutons) == 0 or len(self.moutons)> self.max_mouton:#on verifie si les moutons n'ont pas envahit le monde, si ils l'ont fait ,on met fin a la simulation
                self.horloge = self.fin_du_monde
        return self.resultats_herbe,self.resultats_moutons,self.resultats_loups #on renvoie la liste d'herbe,de mouton et de loup
        
        
        
