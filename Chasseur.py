from math import sqrt
class Chasseur: #cette classe permet de generer une entité chasseur qui va chasser les loups
    def __init__(self,x,y,monde,loup):
        self.position_x = x #initialise sa position x
        self.position_y = y #initialise sa position y
        self.monde = monde #recupere le monde dans lequel il est initialisé
        self.loup = loup #recupere la liste de loup dans lequel il est initialisé
        
    def deplacement(self):#permet de ce deplacer vers le loup le plus proche
        x = sqrt(self.monde.dimension**2+self.monde.dimension**2)
        j = 0
        for i in range(len(self.loup)): #cette fonction va mesure l'indice et la distance du loup le plus proche
            x1 = sqrt((self.loup[i].position_x-self.position_x)**2+(self.loup[i].position_y-self.position_y)**2)
            if x1 < x:
                j = i
                x = x1
        if (self.loup[j-1].position_x-self.position_x)<0: # c'est condition permet de déplacer le chasseur en fonction de la position du loup le plus proche
            self.position_x -= 1
        elif (self.loup[j-1].position_x-self.position_x)>0:
            self.position_x += 1
        if (self.loup[j-1].position_y-self.position_y)<0:
            self.position_y -= 1
        elif (self.loup[j-1].position_y-self.position_y)>0:
            self.position_y += 1
        self.position_x = self.position_x%self.monde.dimension
        self.position_y = self.position_y%self.monde.dimension
