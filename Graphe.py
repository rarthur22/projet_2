import Simulation
import matplotlib.pyplot as plt

def graph(nombre_mouton,fin_du_monde,dimension_m,herbe_pousse,mouton_max): #renvoie le graphe d'une simulation, pour voir le nombre de mouton,de loup et d'herbe en fonction dans la simulation
    z = Simulation.Simulation(nombre_mouton,fin_du_monde,dimension_m,herbe_pousse,mouton_max)
    a,b,c = z.simMouton()
    print(a,b,c)
    x = [i for i in range(len(a))]
    plt.plot(x,a, label = "herbe")
    plt.plot(x,b, label = "mouton")
    plt.plot(x,c, label = "loup")
    plt.legend()
    return plt.show()
