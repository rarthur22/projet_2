from random import randint
class Loup: #cette classe permet de generer une entité loup qui va manger les moutons
    def __init__(self,gain_nour,x,y,t_repro,monde,mouton):
        self.gain_nourriture = gain_nour
        self.position_x = x #initialise sa position x
        self.position_y = y #initialise sa position y
        self.energie = randint(1,gain_nour) #permet d'initialiser l'energie du loup
        self.taux_reproduction = t_repro #permet d'initialiser le taux de reproduction du loup
        self.monde = monde #recupere le monde dans lequel il est initialisé
        self.mouton = mouton #recupere la liste de mouton dans lequel il est initialisé

    def variationEnergie(self): #permet de récupérer l'indice d'un mouton pour l'utiliser dans la simulation
        x = 1
        manger = False
        mouton_m = None
        for i in range(len(self.mouton)):
            if self.mouton[i].position_x == self.position_x and self.mouton[i].position_y == self.position_y and x==1:
                self.energie += self.gain_nourriture
                x -= 1
                manger = True
                mouton_m = i
        if manger == False:
            self.energie -= 1
        return self.energie,mouton_m
        
    def deplacement(self): #permet de se deplacer
        self.position_x += randint(-1,1)
        self.position_y += randint(-1,1)
        self.position_x = self.position_x%self.monde.dimension
        self.position_y = self.position_y%self.monde.dimension
    
    def place_loup(self,i,j): #permet de placer une nouvelle entité loup au coordonnée du loup
        return Loup(self.gain_nourriture,i%self.monde.dimension,j%self.monde.dimension,self.taux_reproduction,self.monde,self.mouton)
