from random import randint
class Mouton: #cette classe permet de generer une entité mouton qui va manger l'herbe
    def __init__(self,gain_nour,x,y,t_repro,monde):
        self.gain_nourriture = gain_nour #initialise le gain de nourriture par herbe mangé
        self.position_x = x #initialise sa position x
        self.position_y = y #initialise sa postion y
        self.energie = randint(1,gain_nour*2) #initialise son energie au début
        self.taux_reproduction = t_repro #initialise son taux de reproduction
        self.monde = monde #recupere le monde dans lequel il est initialisé

    def variationEnergie(self,i,j): #permet d'augmenter de gain_nourriture son energie si il est sur une case herbe, sinon il enleve 1
        if self.monde.getCoefCarte(i%self.monde.dimension,j%self.monde.dimension) >= self.monde.duree_repousse:
            self.energie += self.gain_nourriture
            self.monde.herbeMangee(i%self.monde.dimension,j%self.monde.dimension)
        else:
            self.energie -= 1
        return self.energie
        
    def deplacement(self): #permet de se deplacer
        self.position_x += randint(-1,1)
        self.position_y += randint(-1,1)
        self.position_x = self.position_x%self.monde.dimension
        self.position_y = self.position_y%self.monde.dimension
    
    def place_mouton(self,i,j): #permet de placer une nouvelle entité mouton au coordonnée du mouton
        return Mouton(self.gain_nourriture,i%self.monde.dimension,j%self.monde.dimension,self.taux_reproduction,self.monde)
        
