from random import randint,shuffle
class Monde: #Cette classe permet de generer une matrice que l'on peut appeller monde pour la simulation
    def __init__(self,dimension,duree_repousse): #On ajoute comme paramètre à ce monde ,dimension pour la taille du monde et duree_repousse pour savoir quand une valeur dans la matrice est considérer comme herbe
        self.dimension = dimension #la dimension du monde
        self.duree_repousse = duree_repousse #la duree lorsqu'on considere une valeur en herbe
        carte = [duree_repousse for i in range(((dimension**2)+1)//2)]+[randint(0,duree_repousse-1) for i in range(((dimension**2)-1)//2)] #initialise 50 herbus dans le monde
        shuffle(carte)
        self.carte = []
        for i in range(dimension): # permet de créer un monde
            self.carte.append(carte[i*5:i*5+dimension])
        
    def herbePousse(self): #cette méthode fait pousser += 1 dans la matrice
        for i in range(self.dimension):
            for j in range(self.dimension):
                if self.carte[i][j]<self.duree_repousse:
                    self.carte[i][j] += 1
        
    def herbeMangee(self,i,j): #cette méthode permet de reinitialiser une herbe à 0
        if self.carte[i%self.dimension][j%self.dimension] >= self.duree_repousse:
            self.carte[i%self.dimension][j%self.dimension] = 0
        
    def nbHerbe(self): #cette méthode permet de compter le nombre d'herbe dans le monde
        Herbe = 0
        for i in range(self.dimension):
            for j in range(self.dimension):
                if self.carte[i][j] >= self.duree_repousse:
                    Herbe += 1
        return Herbe
        
    def getCoefCarte(self,i,j): #avoir la valeur dans la matrice(i;j)
        return self.carte[i-1][j-1]
